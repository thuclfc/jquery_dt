/*!
 * jquery_dt
 * 
 * 
 * @author Thuclfc
 * @version 2.0.0
 * Copyright 2019. MIT licensed.
 */$(document).ready(function () {
    $('.slider_img').slick({
        centerMode: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        centerPadding: 0,
        dots: false,
        arrows: false
    });

    $('.slider_img').mousedown(function (e1) {
        var mx = e1.pageX; //register the mouse down position

        $(this).mousemove(function (e2) {

            if (e2.pageX > mx) {
                //right w.r.t mouse down position
                $('.slider-home').addClass('expand');
            } else {
                $('.slider-home').removeClass('expand');
            }
        });
    });
});